import java.util.Scanner;
import java.util.ArrayDeque;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
/*
  Author @lotalorafox
*/


public class Game{
  //colors
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_BLACK = "\u001B[30m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_PURPLE = "\u001B[35m";
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_WHITE = "\u001B[37m";
  public static void main(String[] args) {
    //arrays of colors to print
    String[] colansi = {ANSI_BLUE,ANSI_GREEN,ANSI_RED,ANSI_PURPLE,ANSI_YELLOW};
    Scanner sc = new Scanner(System.in);
    // control variable 1
    boolean c1 =true;
    //dice object
    Dice d = new Dice();
    //players stack with head and tail
    ArrayDeque[] ls = new ArrayDeque[5];
    //options of playesr
    String s = "1 2 3 4 5";
    int p=0;
    //get the numbers of player
    while(c1){
      System.out.println("Insert the number of players");
      String o1 = sc.next();
      if(s.contains(o1)){
        c1=false;
        p = Integer.parseInt(o1);
      }
    }
    //options of colors
    String optio = "ygbrp";
    //playesr to save the names
    HashMap<String,String> players = new HashMap<>();
    //full the players and put the colors
    for(int i=0;i<p;i++){
      //get the name
      System.out.println("insert the name of player " + (i+1));
      String o2 = sc.next();
      //get the color
      System.out.println("What color want the player " + (i+1));
      System.out.println("Y. Yellow, G. Green, B. Blue, R. Red, P. Purple");
      String o3 = sc.next();
      //erify the color
      if((optio.contains(o3.toLowerCase()))){
          //put the player
          if(o3.toLowerCase().charAt(0) == 'y'){
            if(!(players.containsKey(d.colors[4]))){
              players.put(d.colors[4], o2);
            }else{
              System.out.println("that color is already assing");
              i--;
            }
          }else if(o3.toLowerCase().charAt(0) == 'g'){
            if(!(players.containsKey(d.colors[1]))){
              players.put(d.colors[1], o2);
            }else{
              System.out.println("that color is already assing");
              i--;
            }
            //players.put(d.colors[1], o2);
          }else if(o3.toLowerCase().charAt(0) == 'b'){
            if(!(players.containsKey(d.colors[0]))){
              players.put(d.colors[0], o2);
            }else{
              System.out.println("that color is already assing");
              i--;
            }
            //players.put(d.colors[0], o2);
          }else if(o3.toLowerCase().charAt(0) == 'r'){
            if(!(players.containsKey(d.colors[2]))){
              players.put(d.colors[2], o2);
            }else{
              System.out.println("that color is already assing");
              i--;
            }
            //players.put(d.colors[2], o2);
          }else if(o3.toLowerCase().charAt(0) == 'p'){
            if(!(players.containsKey(d.colors[3]))){
              players.put(d.colors[3], o2);
            }else{
              System.out.println("that color is already assing");
              i--;
            }
            //players.put(d.colors[3], o2);
          }
      }else{
        //option not true
        System.out.println("this is not a color");
        i--;
      }
    }
    System.out.println(players.toString());
    boolean aleja = true;
    //add the colors to the stack
    for(int i=0;i<5;i++){
      ls[i] = new ArrayDeque();
      ls[i].add(d.colors[i]);
    }
    //
    String a ="";
    //calculate the colors and the bars
    while(aleja){
      a = d.roll();
      System.out.println("the dice says " + a);
      for (int j=0;j<5 ;j++ ) {
        if(a.equals(ls[j].peek())){
          ls[j].add("*");
          break;
        }else if(a.equals("Black")){
          System.out.println("All lose one");
          for(int i=0;i<5;i++){
            if(ls[i].size() >1){
              ls[i].removeLast();
            }
          }
          break;
        }
      }
      //print the colors and the bars
      for(int i=0;i<5;i++){
        String t = ls[i].toString();
        t =t.replace('[',' ');
        t =t.replace(',',' ');
        t =t.replace(']',' ');
        System.out.println(colansi[i] + t + ANSI_RESET);
      }
      // print the player when it wins
      for(int i=0;i<5;i++){
        if(ls[i].size() == 11){
          //System.out.println(players.toString());
          System.out.println(ls[i].peek() + " and the player " + players.get(ls[i].peek()) + " Wins !!!!");
            aleja = false;
        }
      }
      //do the cicle more slow
      try{
        TimeUnit.MILLISECONDS.sleep(600);
        if(aleja){
          //clean the screen
          clearScreen();
        }
      }catch (Exception e) {
        System.err.println(e.getMessage());
      }

    }
  }
  //print the array
  public static void printArrayDeque(ArrayDeque w){
    ArrayDeque tem = w;
    while(!(tem.isEmpty())){
      System.out.print(tem.peek() + " " );
      tem.remove();
    }
    System.out.println();
  }
  public static void clearScreen() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}
