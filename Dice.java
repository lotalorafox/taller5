import java.util.Random;
//dice clas
public class Dice{
  //array of colors 41 posibilities with 3 blacks
  public String[] colors ={"Blue","Green","Red","Purple","Yellow","Black","Black","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Purple","Yellow","Blue","Green","Red","Black","Purple","Yellow"};

  public Dice(){

  }

  /*roll the dice */

  public String roll(){
    Random rand = new Random();
    int randomNum = rand.nextInt((colors.length-1) + 0);
    return colors[randomNum];
  }

}
